﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PredatorPrey
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const UInt16 LeftTopX = 25;
        const UInt16 LeftTopY = 25;
        const UInt16 RightBottomX = 425;
        const UInt16 RightBottomY = 425;

        const UInt16 GridSize = 20;
        const UInt16 ParticleSize = 10;

        const UInt16 MatrixEmptyValue = 0;
        const UInt16 MatrixPredatorValue = 1;
        const UInt16 MatrixPreyValue = 2;

        UInt16[,] PredPreyMatrix;

        Random random = new Random();

        private List<Point> PhaseSpace = new List<Point>();

        public MainWindow()
        {
            InitializeComponent();
            PredPreyMatrix = new UInt16[GridSize, GridSize];

            DrawGrid();
            InitializePredPrey();
            DrawPredatorsAndPrey();
            DrawPhaseSpace();
        }

        private void DrawGrid()
        {
            for (int index_columns = LeftTopX; index_columns <= RightBottomX; index_columns += 20)
            {
                Line objLine = new Line();
                objLine.Stroke = System.Windows.Media.Brushes.Black;
                objLine.Fill = System.Windows.Media.Brushes.Black;
                objLine.X1 = index_columns;
                objLine.Y1 = LeftTopY;
                objLine.X2 = index_columns;
                objLine.Y2 = RightBottomY;
                MainCanvas.Children.Add(objLine);
            }
            for (int index_lines = LeftTopX; index_lines <= RightBottomY; index_lines += 20)
            {
                Line objLine = new Line();
                objLine.Stroke = System.Windows.Media.Brushes.Black;
                objLine.Fill = System.Windows.Media.Brushes.Black;
                objLine.X1 = LeftTopX;
                objLine.Y1 = index_lines;
                objLine.X2 = RightBottomX;
                objLine.Y2 = index_lines;
                MainCanvas.Children.Add(objLine);
            }
        }

        private void InitializePredPrey()
        {
            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    double PreyPredProb = random.NextDouble();

                    if (PreyPredProb < 0.33)
                    {
                        PredPreyMatrix[index_line, index_col] = MatrixEmptyValue;
                    }
                    else if (PreyPredProb < 0.66)
                    {
                        PredPreyMatrix[index_line, index_col] = MatrixPredatorValue;
                    }
                    else
                    {
                        PredPreyMatrix[index_line, index_col] = MatrixPreyValue;
                    }
                }
            }
            //PredPreyMatrix[2, 2] = MatrixPreyValue;
            //PredPreyMatrix[3, 2] = MatrixPreyValue;
            //PredPreyMatrix[2, 3] = MatrixPreyValue;
            //PredPreyMatrix[2, 4] = MatrixPreyValue;
            //PredPreyMatrix[2, 5] = MatrixPreyValue;
            //PredPreyMatrix[3, 5] = MatrixPreyValue;
            //PredPreyMatrix[3, 3] = MatrixPredatorValue;
            //PredPreyMatrix[3, 4] = MatrixPredatorValue;

            //PredPreyMatrix[12, 5] = MatrixPreyValue;

            //PredPreyMatrix[12, 17] = MatrixPredatorValue;

        }

        private void DrawPredatorsAndPrey()
        {
            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    Ellipse newParticle = new Ellipse();
                    Point particleCoords = new Point();

                    if (PredPreyMatrix[index_line, index_col] == MatrixPreyValue)
                    {
                        newParticle.Stroke = Brushes.Blue;
                    }
                    else if (PredPreyMatrix[index_line, index_col] == MatrixPredatorValue)
                    {
                        newParticle.Stroke = Brushes.Red;
                    }
                    newParticle.StrokeThickness = 2;
                    newParticle.Width = ParticleSize;
                    newParticle.Height = ParticleSize;
                    particleCoords.X = LeftTopX + index_line * GridSize + 5;
                    particleCoords.Y = LeftTopY + index_col * GridSize + 5;

                    MainCanvas.Children.Add(newParticle);
                    Canvas.SetLeft(newParticle, particleCoords.X);
                    Canvas.SetTop(newParticle, particleCoords.Y);
                }
            }
        }

        private void DrawAxis()
        {
            Line axisX = new Line();
            axisX.Stroke = System.Windows.Media.Brushes.Black;
            axisX.Fill = System.Windows.Media.Brushes.Black;
            axisX.X1 = LeftTopX + 500;
            axisX.Y1 = RightBottomY;
            axisX.X2 = RightBottomX + 500;
            axisX.Y2 = RightBottomY;

            // Arrow Axis X
            Line line_1 = new Line();
            line_1.Stroke = System.Windows.Media.Brushes.Black;
            line_1.Fill = System.Windows.Media.Brushes.Black;
            line_1.X1 = RightBottomX + 500;
            line_1.Y1 = RightBottomY;
            line_1.X2 = RightBottomX + 495;
            line_1.Y2 = RightBottomY - 5;

            Line line_2 = new Line();
            line_2.Stroke = System.Windows.Media.Brushes.Black;
            line_2.Fill = System.Windows.Media.Brushes.Black;
            line_2.X1 = RightBottomX + 500;
            line_2.Y1 = RightBottomY;
            line_2.X2 = RightBottomX + 495;
            line_2.Y2 = RightBottomY + 5;

            Line axisY = new Line();
            axisY.Stroke = System.Windows.Media.Brushes.Black;
            axisY.Fill = System.Windows.Media.Brushes.Black;
            axisY.X1 = LeftTopX + 500;
            axisY.Y1 = RightBottomY;
            axisY.X2 = LeftTopX + 500;
            axisY.Y2 = LeftTopY;

            // Arrow Axis Y
            Line line_3 = new Line();
            line_3.Stroke = System.Windows.Media.Brushes.Black;
            line_3.Fill = System.Windows.Media.Brushes.Black;
            line_3.X1 = LeftTopX + 500;
            line_3.Y1 = LeftTopY;
            line_3.X2 = LeftTopX + 495;
            line_3.Y2 = LeftTopY + 5;

            Line line_4 = new Line();
            line_4.Stroke = System.Windows.Media.Brushes.Black;
            line_4.Fill = System.Windows.Media.Brushes.Black;
            line_4.X1 = LeftTopX + 500;
            line_4.Y1 = LeftTopY;
            line_4.X2 = LeftTopX + 505;
            line_4.Y2 = LeftTopY + 5;

            MainCanvas.Children.Add(axisX);
            MainCanvas.Children.Add(line_1);
            MainCanvas.Children.Add(line_2);
            MainCanvas.Children.Add(axisY);
            MainCanvas.Children.Add(line_3);
            MainCanvas.Children.Add(line_4);
        }

        private void DrawPhaseSpace()
        {
            double PreyCounter = 0;
            double PredatorCounter = 0;

            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    if (PredPreyMatrix[index_line, index_col] == MatrixPreyValue)
                    {
                        ++PreyCounter;
                    }
                    if (PredPreyMatrix[index_line, index_col] == MatrixPredatorValue)
                    {
                        ++PredatorCounter;
                    }
                }
            }

            PhaseSpace.Add(new Point(PreyCounter, PredatorCounter));
            
            DrawAxis();

            for (int index = 0; index < PhaseSpace.Count; ++index)
            {
                if (index > 0)
                {
                    Point particleCoords_1 = PhaseSpace.ElementAt(index - 1);
                    Point particleCoords_2 = PhaseSpace.ElementAt(index);

                    Line newLine = new Line();
                    newLine.Stroke = System.Windows.Media.Brushes.Green;
                    newLine.Fill = System.Windows.Media.Brushes.Green;

                    newLine.X1 = particleCoords_1.X + LeftTopX + 500;
                    newLine.Y1 = RightBottomY - particleCoords_1.Y;
                    newLine.X2 = particleCoords_2.X + LeftTopX + 500;
                    newLine.Y2 = RightBottomY - particleCoords_2.Y;

                    MainCanvas.Children.Add(newLine);
                }

                Ellipse newParticle = new Ellipse();
                Point particleCoords = PhaseSpace.ElementAt(index);

                newParticle.Stroke = Brushes.Green;
                newParticle.StrokeThickness = 2;
                newParticle.Width = 6;
                newParticle.Height = 6;

                MainCanvas.Children.Add(newParticle);
                Canvas.SetLeft(newParticle, particleCoords.X + LeftTopX + 500 - 3);
                Canvas.SetTop(newParticle, RightBottomY - particleCoords.Y - 3);
            }

            labelPredators.Content = "Predators:\n" + PredatorCounter.ToString();
            labelPrey.Content = "Prey: " + PreyCounter.ToString();
            labelPredators.Foreground = Brushes.Red;
            labelPrey.Foreground = Brushes.Blue;
        }

        private bool CheckNeighbour(int line, int col)
        {
            if (line < 0 || line >= GridSize || col < 0 || line >= GridSize)
            {
                return false;
            }
            return true;
        }

        private void IterateOnce()
        {
            UInt16[,] NewPredPreyMatrix = new UInt16[GridSize, GridSize];

            // Iteration for Predator
            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    if (PredPreyMatrix[index_line, index_col] == MatrixPredatorValue)
                    {
                        bool PreyExists = false;
                        for (int temp_line = index_line - 1; temp_line <= index_line + 1; ++temp_line)
                        {
                            for (int temp_col = index_col - 1; temp_col <= index_col + 1; ++temp_col)
                            {
                                if ((temp_line == index_line && temp_col == index_col) ||
                                    temp_line < 0 || temp_line >= GridSize || temp_col < 0 || temp_col >= GridSize)
                                {
                                    continue;
                                }
                                if (PredPreyMatrix[temp_line, temp_col] == MatrixPreyValue)
                                {
                                    PreyExists = true;
                                    NewPredPreyMatrix[temp_line, temp_col] = MatrixPredatorValue;
                                }
                            }
                        }
                        if (!PreyExists)
                        {
                            NewPredPreyMatrix[index_line, index_col] = MatrixEmptyValue;
                        }
                        else
                        {
                            NewPredPreyMatrix[index_line, index_col] = MatrixPredatorValue;
                        }
                    }
                }
            }

            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    if (PredPreyMatrix[index_line, index_col] == MatrixPreyValue && NewPredPreyMatrix[index_line, index_col] == MatrixEmptyValue)
                    {
                        NewPredPreyMatrix[index_line, index_col] = MatrixPreyValue;
                    }
                }
            }

            PredPreyMatrix = (UInt16[,])NewPredPreyMatrix.Clone();

            // Iteration for Prey
            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    if (PredPreyMatrix[index_line, index_col] == MatrixPreyValue)
                    {
                        for (int temp_line = index_line - 1; temp_line <= index_line + 1; ++temp_line)
                        {
                            for (int temp_col = index_col - 1; temp_col <= index_col + 1; ++temp_col)
                            {
                                if ((temp_line == index_line && temp_col == index_col) ||
                                    temp_line < 0 || temp_line >= GridSize || temp_col < 0 || temp_col >= GridSize)
                                {
                                    continue;
                                }
                                if (PredPreyMatrix[temp_line, temp_col] == MatrixEmptyValue)
                                {
                                    NewPredPreyMatrix[temp_line, temp_col] = MatrixPreyValue;
                                }
                            }
                        }
                    }
                }
            }

            PredPreyMatrix = (UInt16[,])NewPredPreyMatrix.Clone();

            double probability;
            for (int index_line = 0; index_line < GridSize; ++index_line)
            {
                for (int index_col = 0; index_col < GridSize; ++index_col)
                {
                    if (PredPreyMatrix[index_line, index_col] == MatrixPreyValue)
                    {
                        probability = random.NextDouble();
                        if (probability < double.Parse(textBoxPreyDeathRate.Text))
                        {
                            PredPreyMatrix[index_line, index_col] = MatrixEmptyValue;
                        }
                        continue;
                    }

                    if (PredPreyMatrix[index_line, index_col] == MatrixPredatorValue)
                    {
                        probability = random.NextDouble();
                        if (probability < double.Parse(textBoxPredDeathRate.Text))
                        {
                            PredPreyMatrix[index_line, index_col] = MatrixEmptyValue;
                        }
                        continue;
                    }

                    if (PredPreyMatrix[index_line, index_col] == MatrixEmptyValue)
                    {
                        probability = random.NextDouble();
                        if (probability < double.Parse(textBoxPreyBirthRate.Text))
                        {
                            PredPreyMatrix[index_line, index_col] = MatrixPreyValue;
                        }
                        continue;
                    }
                }
            }

            MainCanvas.Children.Clear();
            MainCanvas.Children.Add(labelPredators);
            MainCanvas.Children.Add(labelPrey);
            DrawGrid();
            DrawPredatorsAndPrey();
            DrawPhaseSpace();
        }

        private void buttonOneGen_Click(object sender, RoutedEventArgs e)
        {
            IterateOnce();
        }

        private void buttonNGen_Click(object sender, RoutedEventArgs e)
        {
            for (int iterator = 0; iterator < Int32.Parse(textBoxNGenerations.Text); ++iterator)
            {
                IterateOnce();
            }
        }
    }
}
